FROM java:8
VOLUME /tmp
COPY target/teste_jenkins.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Xms128m","-Xmx512m","-jar","/app.jar"]
EXPOSE 8080
